package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MainController;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class MainView extends JFrame {

	private JPanel contentPane;
	private JButton btnInsert;
	private JButton btnRead;
	private JButton btnUpdate;
	private JButton btnDelete;

	//Vista principal. Cada boton llama a los metodos de visibilizacion de las vistas del controller
	public MainView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		
		btnInsert = new JButton("Insert");
		contentPane.add(btnInsert);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainController.showInsertView();
			}
		});
		
		btnRead = new JButton("Read");
		contentPane.add(btnRead);
		btnRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainController.showReadView();
			}
		});
		
		btnUpdate = new JButton("Update");
		contentPane.add(btnUpdate);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainController.showUpdateView();
			}
		});
		
		btnDelete = new JButton("Delete");
		contentPane.add(btnDelete);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainController.showDeleteView();
			}
		});
	}
}
