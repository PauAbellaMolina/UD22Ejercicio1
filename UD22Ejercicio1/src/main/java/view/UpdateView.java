package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MainController;
import model.dto.Cliente;

import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class UpdateView extends JFrame {

	private JPanel contentPane;
	private JTextField inputId;
	private JTextField inputNombre;
	private JTextField inputApellido;
	private JTextField inputDni;
	private JTextField inputDireccion;
	private JTextField inputFecha;

	public UpdateView() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel lblId = new JLabel("Id");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblId, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblId, 10, SpringLayout.WEST, contentPane);
		contentPane.add(lblId);
		
		inputId = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputId, 6, SpringLayout.SOUTH, lblId);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputId, 0, SpringLayout.WEST, lblId);
		contentPane.add(inputId);
		inputId.setColumns(10);
		
		JLabel lblNewData = new JLabel("New data:");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNewData, 18, SpringLayout.SOUTH, inputId);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNewData, 0, SpringLayout.WEST, lblId);
		contentPane.add(lblNewData);
		
		JLabel lblNombre = new JLabel("Nombre");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblNombre, 9, SpringLayout.SOUTH, lblNewData);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNombre, 0, SpringLayout.WEST, lblId);
		contentPane.add(lblNombre);
		
		inputNombre = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputNombre, 4, SpringLayout.SOUTH, lblNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputNombre, 0, SpringLayout.WEST, lblId);
		contentPane.add(inputNombre);
		inputNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		sl_contentPane.putConstraint(SpringLayout.WEST, lblApellido, 0, SpringLayout.WEST, lblId);
		contentPane.add(lblApellido);
		
		inputApellido = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblApellido, -6, SpringLayout.NORTH, inputApellido);
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputApellido, 166, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputApellido, 0, SpringLayout.WEST, lblId);
		contentPane.add(inputApellido);
		inputApellido.setColumns(10);
		
		JLabel lblDni = new JLabel("Dni");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblDni, 0, SpringLayout.NORTH, lblApellido);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblDni, -26, SpringLayout.SOUTH, inputApellido);
		contentPane.add(lblDni);
		
		inputDni = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, lblDni, 0, SpringLayout.WEST, inputDni);
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputDni, 0, SpringLayout.NORTH, inputApellido);
		contentPane.add(inputDni);
		inputDni.setColumns(10);
		
		JLabel lblDireccion = new JLabel("Direccion");
		sl_contentPane.putConstraint(SpringLayout.WEST, inputDni, 0, SpringLayout.WEST, lblDireccion);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblDireccion, 112, SpringLayout.EAST, lblNombre);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblDireccion, 0, SpringLayout.SOUTH, lblNombre);
		contentPane.add(lblDireccion);
		
		inputDireccion = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputDireccion, 0, SpringLayout.NORTH, inputNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputDireccion, 0, SpringLayout.WEST, lblDireccion);
		contentPane.add(inputDireccion);
		inputDireccion.setColumns(10);
		
		JButton btnUpdate = new JButton("Update");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnUpdate, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnUpdate, -10, SpringLayout.EAST, contentPane);
		contentPane.add(btnUpdate);
		
		inputFecha = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputFecha, 0, SpringLayout.NORTH, inputNombre);
		inputFecha.setColumns(10);
		contentPane.add(inputFecha);
		
		JLabel lblFecha = new JLabel("Fecha");
		sl_contentPane.putConstraint(SpringLayout.EAST, lblFecha, -88, SpringLayout.EAST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputFecha, 0, SpringLayout.WEST, lblFecha);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblFecha, 0, SpringLayout.SOUTH, lblNombre);
		contentPane.add(lblFecha);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Creamos un objeto cliente, le setteamos los datos y se lo pasamos al metodo correspondiente del controller
				Cliente newCliente = new Cliente();
				newCliente.setId(Integer.parseInt(inputId.getText()));
				newCliente.setNombre(inputNombre.getText());
				newCliente.setApellido(inputApellido.getText());
				newCliente.setDireccion(inputDireccion.getText());
				newCliente.setDni(inputDni.getText());
				newCliente.setFecha(inputFecha.getText());
				
				MainController.updateCliente(newCliente);
			}
		});
	}

}
