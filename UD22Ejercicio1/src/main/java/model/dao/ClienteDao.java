package model.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import model.connection.MyConnection;
import model.dto.Cliente;
import view.ReadView;

public class ClienteDao {
	
	//Metodo de insercion de cliente que construye la query y la executa
	public void insertCliente(Cliente newCliente) {
		MyConnection connection = new MyConnection();
		
		try {
			Statement st = connection.getConn().createStatement();
			String sql= "INSERT INTO cliente (nombre, apellido, direccion, dni, fecha) VALUES ('" + newCliente.getNombre() + "','" + newCliente.getApellido() + "','" + newCliente.getDireccion() + "'," + newCliente.getDni() + ", '" + newCliente.getFecha() + "');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha insertado correctamente un nuevo cliente","Insert",JOptionPane.INFORMATION_MESSAGE);
			st.close();
			connection.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha insertado un nuevo cliente");
		}
	}

	//Metodo de update de cliente que construye la query y la executa
	public void updateCliente(Cliente newCliente) {
		MyConnection connection = new MyConnection();
		
		try {
			Statement st = connection.getConn().createStatement();
			String sql= "UPDATE cliente SET nombre = '" + newCliente.getNombre() + "', apellido = '" + newCliente.getApellido() + "', direccion = '" + newCliente.getDireccion() + "', dni = " + newCliente.getDni() + ", fecha = '" + newCliente.getFecha() + "' WHERE id = " + newCliente.getId() + ";";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha actualizado correctamente el cliente","Update",JOptionPane.INFORMATION_MESSAGE);
			st.close();
			connection.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha actualizado el cliente");
		}
	}
	
	//Metodo de lectura de cliente que construye la query, la ejecuta y escribe el resultado en el campo de texto de resultado
	public void readCliente(Cliente newCliente) {
		MyConnection connection = new MyConnection();
		
		try {
			String sql= "SELECT * FROM cliente WHERE id = " + newCliente.getId() + ";";
			Statement st = connection.getConn().createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
			{
				ReadView.txtAreaResult.setText(ReadView.txtAreaResult.getText() + rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5) + " " + rs.getString(6) + "\n");
			}
			st.close();
			connection.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "Read incorrecto");
		}
	}
	
	//Metodo de eliminacion de cliente que construye la query y la executa
	public void deleteCliente(Cliente newCliente) {
		MyConnection connection = new MyConnection();
		
		try {
			Statement st = connection.getConn().createStatement();
			String sql= "DELETE FROM cliente WHERE id = " + newCliente.getId() + ";";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha borrado correctamente el cliente","Update",JOptionPane.INFORMATION_MESSAGE);
			st.close();
			connection.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se ha borrado el cliente");
		}
	}
}
